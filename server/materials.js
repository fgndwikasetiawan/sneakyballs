var CANNON = require('cannon');

var Materials = {}
Materials.GROUND = new CANNON.Material();
Materials.RUBBER = new CANNON.Material();
Materials.GLASS = new CANNON.Material();
Materials.CONTACT_MATERIALS = [
	//GROUND >< RUBBER
	new CANNON.ContactMaterial(Materials.GROUND, Materials.RUBBER, {
		friction: 1,
		restitution: 0.7
	}),
	//GROUND >< GLASS
	new CANNON.ContactMaterial(Materials.GROUND, Materials.GLASS, {
		friction: 0.5,
		restitution: 0.3
	}),
	//RUBBER >< RUBBER
	new CANNON.ContactMaterial(Materials.RUBBER, Materials.RUBBER, {
		friction: 0,
		restitution: 1
	}),
	//GLASS >< GLASS
	new CANNON.ContactMaterial(Materials.GLASS, Materials.GLASS, {
		friction: 0,
		restitution: 1
	})
];

module.exports = Materials;
