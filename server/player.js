var CANNON = require('cannon');

var _id = 0;

function Player(socket, name) {
	this.socket = socket;
	this.torqueX = 0;
	this.torqueY = 0;
	this.torqueZ = 0;
	this.id = _id++;
	this.score = 0;
	this.ball = null;
	this.name = name;
	this.powerup = null;
	this.isDead = false;
} 

Player.prototype.incrementScoreBy = function(num) {
	this.score += num;
}

Player.prototype.setBall = function(ball) {
	this.ball = ball;
}

Player.prototype.setBallTorque = function(torqueX, torqueY, torqueZ) {
	this.torqueX = torqueX;
	this.torqueY = torqueY;
	this.torqueZ = torqueZ;
}

Player.prototype.applyBallTorque = function() {
	try {
		this.ball.body.torque = new CANNON.Vec3(this.torqueX, this.torqueY, this.torqueZ);
	}
	catch (ex) {
	}
}


module.exports = Player;