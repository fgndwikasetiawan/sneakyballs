var CANNON = require('cannon');
var Materials = require('./materials.js');
var Utils = require('./utils.js');
var Ball = require('./ball.js');
var Player = require('./player.js');
var Explosion = require('./explosion.js');
var Powerups = require('./powerups.js');

var AI = {
	Peasant: require('./ai/peasant.js')
}

var REINITIALIZE_TIMEOUT = 30000;
var DEFAULT_ROUND_TIME = 180; 
var POWERUP_COOLDOWN = 45;

function Game() {
	this.STEP = 1/60;
	this.MAX_TORQUE = 30;
	
	this.roundTime = DEFAULT_ROUND_TIME;
	this.generatePowerupTime = this.roundTime - POWERUP_COOLDOWN;
	this.intervalId = null;
	this.isPaused = true;
	this.balls = [];
	this.players = [];
	this.watchers = [];
	this.ais = [];
	this.explosions = [];
	this.respawns = [];
	this.states = {};
	this.isNotStartedYet = true;
	this.hasEnded = false;
	//this.explosions = [];
	
	this.world = new CANNON.World();
	this.world.gravity.set(0, -30, 0);
	this.initWorld();
};

Game.prototype.reinitialize = function() {
	this.pause('init');
	this.roundTime = DEFAULT_ROUND_TIME;
	this.isNotStartedYet = true;
	this.hasEnded = false;
	this.balls = [];
	this.respawns = [];
	this.ais = [];
	this.world = new CANNON.World();
	this.world.gravity.set(0, -30, 0);
	this.initWorld();
	for (var i in this.players) {
		this.players[i].score = 0;
		this.assignBallToPlayer(this.players[i]);
	}
	for (var i in this.balls) {
		console.log('\t' + this.balls[i].id);
	}
	this.broadcastPlayerScores();
	console.log('Game Reinitialized');
}

Game.prototype.initWorld = function() {
	for (var i in Materials.CONTACT_MATERIALS) {
		this.world.addContactMaterial(Materials.CONTACT_MATERIALS[i]);	
	}
	
	this.ground = makeGround();
	for (var i=0; i<10; i++) {
		this.addNewBall(new CANNON.Vec3(Math.random() * 30 - 15, Math.random() * 8 + 2, Math.random() * 30 - 15));
	}
	for (var i in this.balls) {
		this.balls[i].ai = new AI.Peasant(this.balls[i], this, null);
		this.ais.push(this.balls[i].ai);
	}
	this.world.addBody(this.ground);
}

Game.prototype.update = function(step) {
	//cek roundTime
	this.roundTime -= step;
	if (this.roundTime <= 0) {
		var isEnded = this.endRound();
		this.roundTime = 0;
		if (isEnded) {
			return;
		}
	}
	//pre-step
	//cek apakah waktunya generate powerup
	if (Math.round(this.roundTime) == Math.round(this.generatePowerupTime)) {
		this.generatePowerupTime -= POWERUP_COOLDOWN;
		for (var i in this.players) {
			this.givePowerupTo(this.players[i]);
			this.broadcastToWatchers('ppg', {id: this.players[i].id, p: this.players[i].powerup});
			console.log(this.players[i].name + ' got ' + this.players[i].powerup);
		}
	}
	//cek apakah ada yg perlu di-respawn
	while(this.respawns[0] && this.respawns[0].time >= Math.round(this.roundTime)) {
		console.log('respawning ball #' + this.respawns[0].ball.id);
		this.respawnBall(this.respawns[0].ball);
		if (this.respawns[0].player) {
			console.log('|__respawning player ' + this.respawns[0].player.name);
			this.assignBallToPlayer(this.respawns[0].player);
		}
		for (var i in this.respawns) {
			console.log('\t' + this.respawns[i].ball.id + ': ' + this.respawns[i].time);
		}
		this.respawns.splice(0, 1);
	}
	//apply torque-nya controller
	for (var i in this.players) {
		this.players[i].applyBallTorque();
	}
	//jalankan ai
	for (var i in this.ais) {
		this.ais[i].run(step);
	}	
	//step
	this.world.step(step);
	//post-step
	var ballInfos = [];
	for (var i in this.balls) {
		ballInfos.push(this.balls[i].getBallInfo());
	}
	var explosionInfos = [];
	for (var i in this.explosions) {
		explosionInfos.push(this.explosions[i].getExplosionInfo())
	}
	this.states = {t: Math.round(this.roundTime), bi: ballInfos, ei: explosionInfos};
}

Game.prototype.endRound = function() {
	if (this.players.length > 0) {
		var endData = {};
		var tie = false;
		var winningPlayer = this.players[0];
		for (var i in this.players) {
			if (this.players[i].score > winningPlayer.score) {
				tie = false;
				winningPlayer = this.players[i];
			}
			else if (this.players[i].score == winningPlayer.score) {
				// tie = true;
			}
		}
		if (tie) {
			return false;
		}
		else {
			endData.winner = winningPlayer.name;
			this.hasEnded = true;
			this.states = {t:0};
			for (var i in this.watchers) {
				this.watchers[i].emit('end', endData);
			}
		}
		clearInterval(this.intervalId);
		this.intervalId = null;
		this.isPaused = true;
		return true;	
	}
}

Game.prototype.updateWatchers = function(data) {
	for(var i in this.watchers) {
		this.watchers[i].emit('u', data);
	}
}

Game.prototype.assignBallToPlayer = function(player) {
	if (this.balls.length > 0 && this.players.length < this.balls.length) {
		var i = Math.round(Math.random() * (this.balls.length-1));
		while (this.balls[i].player != null) {
			i = Math.round(Math.random() * (this.balls.length-1));
		}
		if (this.balls[i].ai) {
			var index = this.ais.indexOf(this.balls[i].ai);
			this.ais.splice(index, 1);
			this.balls[i].ai = null;
		}
		player.setBall(this.balls[i]);
		this.balls[i].player = player;
	}
	else {
		console.log("Error assigning ball: no free balls left");
	}
}

Game.prototype.addPlayer = function(socket, name) {
	var player = new Player(socket, name);
	this.assignBallToPlayer(player);
	var game = this;
	
	player.socket.on('u', function(isi) {
		try {
			isi = JSON.parse(isi);
			game.handlePlayerUpdate(player, isi);
		}
		catch(e) {
		}
	});
	player.socket.on('j', function() {
		game.handlePlayerJump(player);
	});
	player.socket.on('e', function() {
		game.handlePlayerExplode(player);
	});
	player.socket.on('p', function(){
		game.handlePlayerUsePowerup(player);
	});
	player.socket.on('disconnect', function(){
		game.handlePlayerDisconnect(player);
	});
	
	this.players.push(player);
	
	for (var i in this.watchers) {
		this.watchers[i].emit('pc', {id: player.id, name: player.name});
	}
}

Game.prototype.removePlayer = function(player) {
	var index = this.players.indexOf(player);
	this.players.splice(index, 1);
	player.ball.ai = new AI.Peasant(player.ball, this, null);
	this.ais.push(player.ball.ai);
}

Game.prototype.addWatcher = function(watcher) {
	var game = this;
	clearTimeout(this.reinitializationTimeout)
	if (this.isNotStartedYet) {
		watcher.emit('pause', {type: 'init'});
	}
	else {
		watcher.emit(this.isPaused ? 'pause' : 'resume', {}); 
	}
	for (var i in this.players) {
		watcher.emit('pc', {id: this.players[i].id, name: this.players[i].name});
	}
	this.watchers.push(watcher);
	this.broadcastPlayerScores();
	watcher.on('resume', function(){
		console.log('resume, hasEnded: ' + game.hasEnded);
		if (!game.hasEnded) {
			game.resume();	
			game.isNotStartedYet = false;
		}
	});
	watcher.on('pause', function(){
		console.log('pause, hasEnded: ' + game.hasEnded);
		if (!game.hasEnded) {
			game.pause();
		}
	});
	watcher.on('restart', function() {
		console.log('got restart');
		if (game.hasEnded) {
			console.log('renitializing');
			game.reinitialize();
		}
	});
	watcher.on('disconnect', function() {
		game.handleWatcherDisconnect(watcher);
	});
}

Game.prototype.removeWatcher = function(watcher) {
	var index = this.watchers.indexOf(watcher);
	this.watchers.splice(index, 1);
}

Game.prototype.handlePlayerDisconnect = function(player) {
	this.removePlayer(player);
	for (var i in this.watchers) {
		this.watchers[i].emit('pd', player.id);
	}
}

Game.prototype.handleWatcherDisconnect = function(watcher) {
	var game = this;
	this.removeWatcher(watcher);
	if (this.watchers.length == 0) {
		game.pause();
		game.reinitializationTimeout = setTimeout(function(){game.reinitialize()}, REINITIALIZE_TIMEOUT);
	}
}

Game.prototype.handlePlayerUpdate = function(player, vector) {
	var x = vector.x * -1;
	var y = vector.y;
	var torqueZ = x * this.MAX_TORQUE;
	var torqueX = y * this.MAX_TORQUE;
	if (player.ball !== null) {
		player.setBallTorque(torqueX, 0, torqueZ);
	}
}

Game.prototype.handlePlayerJump = function(player) {
	if (player.ball)
		player.ball.jump();	
}

Game.prototype.handlePlayerUsePowerup = function(player) {
	var powerup = player.powerup;
	switch (powerup) {
		case Powerups.INSTA_BOMB:
			if (this.handlePlayerExplode(player, true)) {	
				player.powerup = null;
				this.broadcastToWatchers('ppu', {id: player.id, p: powerup});
			}
			break;
		case Powerups.SWAP:
			var oldBall = player.ball;
			if (oldBall) { //kalau punya bola (tidak mati)
				this.assignBallToPlayer(player);
				oldBall.player = null;
				oldBall.gotHitBy = null;
				oldBall.ai = new AI.Peasant(oldBall, this, null);
				this.ais.push(oldBall.ai);
				player.powerup = null;
				this.broadcastToWatchers('ppu', {id: player.id, p: powerup});
			}
			break;
	}
}

Game.prototype.handlePlayerExplode = function(player, instaBomb) {
	var game = this;
	if (player.ball && !player.ball.isTicking) {
		var ball = player.ball;
		player.ball.startTicking(instaBomb? 0 : 2000, function(){game.handleBallExplode(ball)});
		return true;
	}
	else {
		return false;
	}
}

Game.prototype.handleBallExplode = function(ball) {
	var game = this;
	
	for (var i in this.watchers) {
		this.watchers[i].emit('e', ball.id);
	}
	var player = ball.player;
	var ballPos = ball.body.position;
	this.handleBallDeath(ball, true);
	var explosion = new Explosion(player, new CANNON.Vec3(ballPos.x, ballPos.y - 2, ballPos.z), 15, 250, function() {	
		var explosionIndex = game.explosions.indexOf(explosion);
		game.explosions.splice(explosionIndex, 1);
		game.world.removeBody(explosion.body);
		game.broadcastToWatchers('ee', explosion.id);
	});
	this.explosions.push(explosion);
	this.world.addBody(explosion.body);
}

Game.prototype.handleBallDeath = function(ball, isExploding) {
	var game = this;
	
	for (var i in this.watchers) {
		this.watchers[i].emit('d', ball.id);
	}
	
	var player = ball.player;
	var killer = ball.gotHit ? ball.gotHit.by : null;
	this.killBall(ball);
	if (player) {
		player.setBall(null);
		if (killer) {
			console.log(player.name + " is killed by " + killer.name);
			killer.incrementScoreBy(2);
			this.broadcastPlayerScores();
		}
		else {
			console.log(player.name + " commited suicide");
			player.incrementScoreBy(isExploding ? -1 : -2);
		}
		this.broadcastPlayerScores();
	}
	
	this.respawns.push({time: Math.round(this.roundTime - 5), ball: ball, player: player});
	console.log('--------------------------------------');
	console.log('ball ' + ball.id + ' dieded @ ' + this.roundTime);
	for (var i in this.respawns) {
		console.log('\t' + this.respawns[i].ball.id + ': ' + this.respawns[i].time);
	}
	console.log('--------------------------------------');

}

Game.prototype.respawnBall = function(ball) {
	if (!ball.player && !ball.ai) {
		ball.ai = new AI.Peasant(ball, this, null);
		this.ais.push(ball.ai);
	} 
	ball.body.position = new CANNON.Vec3(Math.random() * 30 - 15, Math.random() * 8 + 2, Math.random() * 30 -15);
	ball.reinitialize();
	ball.body.type = CANNON.Body.DYNAMIC;
}

Game.prototype.killBall = function(ball) {
	ball.isDead = true;
	if (ball.player) {
		ball.player.setBallTorque(0, 0, 0);
		ball.player = null;
	}
	ball.body.type = CANNON.Body.STATIC;
	ball.body.position = new CANNON.Vec3(-1000, -1000, -1000); //xD
	
}

Game.prototype.resume = function() {
	if (this.intervalId === null) {
		this.intervalId = setInterval(function(_this){
			_this.update(_this.STEP);
			_this.updateWatchers(_this.states);
		}, this.STEP * 1000, this); //wtf syntax
	}
	this.isPaused = false;
	for (var i in this.balls) {
		this.balls[i].resume();
	}
	for (var i in this.watchers) {
		this.watchers[i].emit('resume', {});
	}
}

Game.prototype.pause = function(type) {
	clearInterval(this.intervalId);
	this.intervalId = null;
	this.isPaused = true;
	for (var i in this.balls) {
		this.balls[i].pause();
	}
	for (var i in this.watchers) {
		this.watchers[i].emit('pause', {type: type});
	}
}

Game.prototype.broadcastToWatchers = function(name, data) {
	for (var i in this.watchers) {
		this.watchers[i].emit(name, data);
	}
}

Game.prototype.broadcastPlayerScores = function() {
	var scores = [];
	for (var i in this.players) {
		scores.push({id: this.players[i].id, s: this.players[i].score});
	}
	for (var i in this.watchers) {
		this.watchers[i].emit('ps', scores);
	}
}

Game.prototype.givePowerupTo = function(player, powerup) {
	powerup = powerup ? powerup : Powerups.Array[Math.round(Math.random() * (Powerups.Array.length-1))];
	player.powerup = powerup;
}

Game.prototype.addNewBall = function (position, rotation, material, mass) {
	var game = this;
	var ball = new Ball(position, rotation, material, mass, function(){game.handleBallDeath(ball, false)}, this.ground.deathZoneFunction);
	this.balls.push(ball);
	this.world.addBody(ball.body);
}

Game.prototype.removeBall = function(ball) {
	this.world.removeBody(ball);
	var ballIndex = this.balls.indexOf(ball);
	this.balls.splice(ballIndex, 1);
}

Game.prototype.getBalls = function() {
	return this.balls;
}

Game.prototype.getGround = function() {
	return this.ground;
}

Game.prototype.getStep = function() {
	return this.STEP;
}

Game.prototype.getMaxControllerTorque = function() {
	return this.MAX_TORQUE;
}
/////////////////////////////////////////////////////////
///____private functions
/////////////////////////////////////////////////////////


function makeGround() {
	var ground = new CANNON.Body({type: CANNON.Body.STATIC, material: Materials.GROUND});
	ground.collisionFilterGroup = Utils.CollisionFilterGroups.GROUND;
	ground.collisionFilterMask = Utils.CollisionFilterMasks.ALL;
	ground.addShape(new CANNON.Box(new CANNON.Vec3(25, 0.5, 25)));
	ground.position = new CANNON.Vec3(0, 0, 0);
	ground.isGround = true;
	ground.aiDimension = {x: 50, z: 50};
	ground.deathZoneFunction = function(vector3) {
		if (vector3.x < -26 || vector3.x > 26 || vector3.y < -26 || vector3.y > 26) {
			return true;
		}
		return false;
	}
	return ground;	
}



module.exports = new Game();