var CANNON = require('cannon');
var Utils = require('../utils.js');

var TARGET_RADIUS = 5;
var MAX_ACTIVENESS = 5;

var KIRI_ATAS = 0;
var ATAS = 1;
var KANAN_ATAS = 2;
var KIRI = 3;
var TENGAH = 4;
var KANAN = 5;
var KIRI_BAWAH = 6;
var BAWAH = 7;
var KANAN_BAWAH = 8;

function makeCollisionGrids(_this, grids, perception) {

	for (var i in perception.proximateCollisions) {
		var velocity = perception.proximateCollisions[i].velocity;
		var ballDistanceX = perception.proximateCollisions[i].ball.body.position.x - _this.ball.body.position.x;
		var ballDistanceZ = perception.proximateCollisions[i].ball.body.position.z - _this.ball.body.position.z;
		var ballRadius = _this.ball.body.shapes[0].radius;
		if (ballDistanceX < -ballRadius) {
			if (ballDistanceZ < -ballRadius) {
				grids[KIRI_ATAS] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KIRI_ATAS);
			}
			else if (ballDistanceZ > ballRadius) {
				grids[KIRI_BAWAH] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KIRI_BAWAH);
			}
			else {
				grids[KIRI] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KIRI);
			}
		}
		else if (ballDistanceX > ballRadius) {
			if (ballDistanceZ < -ballRadius) {
				grids[KANAN_ATAS] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KANAN_ATAS);
			}
			else if (ballDistanceZ > ballRadius) {
				grids[KANAN_BAWAH] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KANAN_BAWAH);
			}
			else {
				grids[KANAN] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, KANAN);		
			}
		}
		else {
			if (ballDistanceZ < -ballRadius) {
				grids[ATAS] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, ATAS);
			}
			else if (ballDistanceZ > ballRadius) {
				grids[BAWAH] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, BAWAH);
			}
			else {
				grids[TENGAH] = 1;
				traceLine(grids, _this.ball, velocity, perception.proximateCollisions[i].ball.body.position, TENGAH);	
			}
		}
	}
}


function traceLine(grids, ball, velocity, movingBallPosition, currentGrid) {
	grids[currentGrid]++;

	//helper variables;
	var r,x,z,mx,mz,a,b;
	r = ball.body.shapes[0].radius;
	x = ball.body.position.x;
	z = ball.body.position.z;
	mx = movingBallPosition.x;
	mz = movingBallPosition.z;

	switch(currentGrid) {
		case KIRI_ATAS:
			if (velocity.x > 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b < (z - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), ATAS)
				}
			}
			if (velocity.z > 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b < (x - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KIRI)
				}
			}
			break;
		case ATAS:
			if (velocity.x > 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b < (z - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KANAN_ATAS)
				}
			}
			else if (velocity.x < 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b < (z - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KIRI_ATAS)
				}
			}
			if (velocity.z > 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x - r) && b < (x + r) ) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), TENGAH)
				}
			}
			break;
		case KANAN_ATAS:
			if (velocity.x < 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b < (z - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KIRI)
				}
			}
			if (velocity.z > 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KANAN)
				}
			}
			break;
		case KIRI:
			if (velocity.x > 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z - r) && b < (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), TENGAH)
				}
			}
			if (velocity.z < 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b < (x - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KIRI_ATAS)
				}
			}
			else if (velocity.z > 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b < (x - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KIRI_BAWAH)
				}
			}
			break;
		case TENGAH:
			if (velocity.x < 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z - r) && b < (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KIRI)
				}
			}
			else if (velocity.x > 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z - r) && b < (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KANAN)
				}
			}
			if (velocity.z < 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x - r) && b < (x + r) ) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), ATAS)
				}
			}
			else if (velocity.z > 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x - r) && b < (x + r) ) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), BAWAH)
				}
			}
			break;
		case KANAN:
			if (velocity.x < 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z - r) && b < (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), TENGAH)
				}
			}
			if (velocity.z < 0) {
				a = z - r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KANAN_ATAS)
				}
			}
			else if (velocity.z > 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KANAN_BAWAH)
				}
			}
			break;
		case KIRI_BAWAH:
			if (velocity.x > 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), BAWAH)
				}
			}
			if (velocity.z < 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b < (x - r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), KIRI)
				}
			}
			break;
		case BAWAH:
			if (velocity.x > 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KANAN_BAWAH)
				}
			}
			else if (velocity.x < 0) {
				a = x - r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), KIRI_BAWAH)
				}
			}
			if (velocity.z < 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x - r) && b < (x + r) ) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), TENGAH)
				}
			}
			break;
		case KANAN_BAWAH:
			if (velocity.x < 0) {
				a = x + r - mx;
				b = (a / velocity.x * velocity.z) + mz;
				if (b > (z + r)) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(a, 0, b), BAWAH)
				}
			}
			if (velocity.z < 0) {
				a = z + r - mz;
				b = (a / velocity.z * velocity.x) + mx;
				if (b > (x + r) ) {
					traceLine(grids, ball, velocity, new CANNON.Vec3(b, 0, a), TENGAH)
				}
			}
			break;
	}
}

function findBestVector(grids) {
	var vectorX = grids[KIRI_ATAS] + grids[KIRI_BAWAH] + 2 * grids[KIRI] 
					  - grids[KANAN_ATAS] - grids[KANAN_BAWAH] - 2 * grids[KANAN];
	var vectorZ = grids[KIRI_ATAS] + grids[KANAN_ATAS] + 2 * grids[ATAS]
					  - grids[KIRI_BAWAH] - grids[KANAN_BAWAH] - 2 * grids[BAWAH]; 
	return new CANNON.Vec3(vectorX, 0, vectorZ);
}
	

function Peasant(ball, game, params) {
	this.ball = ball;
	this.game = game;
	this.goals = [];
	params = params ? params : {};
	this.activeness = params.activeness ? params.activeness : Math.random() * MAX_ACTIVENESS;
}


Peasant.prototype.run = function() {
	var perception = this.percept();
	var action = this.think(perception);
	this.act(action);
}

Peasant.prototype.percept = function() {
	var perception = {};
	perception.passedTime = this.game.getStep();
	perception.selfVelocity = this.ball.body.velocity.scale(perception.passedTime);
	var balls = this.game.getBalls();
	perception.proximateCollisions = [];
	perception.ballVelocity = [];
	for (var i in balls) {
		if (balls[i] == this.ball) {
			continue;
		}
		//velocity
		perception.ballVelocity[balls[i].id] = balls[i].body.velocity.scale(perception.passedTime);
		//proximateCollision
		var ballNextPosition = balls[i].body.position.vadd(perception.ballVelocity[balls[i].id].scale(10));
		var selfNextPosition = this.ball.body.position.vadd(perception.selfVelocity.scale(10));
		var distance = ballNextPosition.distanceTo(selfNextPosition);
		if (distance <= (this.ball.body.shapes[0].radius + balls[i].body.shapes[0].radius) * 1.5) {
			perception.proximateCollisions.push({ball: balls[i], velocity: perception.ballVelocity[balls[i].id]});
		}
		//cek tepi ground
		var margin = (this.ball.body.shapes[0].radius) * 2;
		var groundDimension = this.game.getGround().aiDimension;
		var leftEdge = (groundDimension.x / 2 * -1) + margin;
		var rightEdge = (groundDimension.x / 2) - margin;
		var topEdge = (groundDimension.z / 2 * -1) + margin;
		var bottomEdge = (groundDimension.z / 2) - margin;
		selfNextPosition = selfNextPosition.scale(0.8);
		if (selfNextPosition.x <= leftEdge) {
			perception.nearLeftEdge = true;
		}
		else if (selfNextPosition.x >= rightEdge) {
			perception.nearRightEdge = true;
		}
		if (selfNextPosition.z <= topEdge) {
			perception.nearTopEdge = true;
		} 
		else if (selfNextPosition.z >= bottomEdge) {
			perception.nearBottomEdge = true;
		}
	}
	return perception;
}

Peasant.prototype.think = function (perception) {
	var action = {type: '', params: {}}

	if (this.goals.length == 0) {
		this.newTargetPointGoal(perception);
	}

	var goal = this.goals.pop();
	switch (goal.type) {
		case 'targetPoint':
			goal.params.duration -= perception.passedTime;
			if (goal.params.duration > 0) {
				if (this.ball.body.position.distanceTo(goal.params.target) < TARGET_RADIUS) {
					action.type = 'brake';
					if (!goal.params.nextTargetDelay) {
						goal.params.nextTargetDelay = 4 - (Math.random() * this.activeness / MAX_ACTIVENESS) * 3;
						this.goals.push(goal);
					}
					else {
						goal.params.nextTargetDelay -= perception.passedTime;
						if (goal.params.nextTargetDelay > 0) {
							this.goals.push(goal);
						}
						else {
							this.newTargetPointGoal(perception);
						}
					}
				}
				else {				
					if (perception.proximateCollisions.length > 0) {
						var grids = [0, 0, 0, 0, 0, 0, 0, 0, 0];
						makeCollisionGrids(this, grids, perception);
						action.type = 'roll'; 
						action.params.vector = findBestVector(grids);
						action.params.forceMaxSpeed = true;
					}
					else {
						action.type = 'roll';
						action.params.vector = goal.params.target.vsub(this.ball.body.position);
					}
					this.goals.push(goal);
				}
			}
	}
	
	if (action.type == 'roll') {
		if (perception.nearLeftEdge) {
			action.params.vector.x = Math.abs(action.params.vector.z + 1) * 2;
		}
		else if (perception.nearRightEdge) {
			action.params.vector.x = Math.abs(action.params.vector.z + 1) * -2;
		}
		if (perception.nearTopEdge) {
			action.params.vector.z = Math.abs(action.params.vector.x + 1) * 2;
		}
		else if (perception.nearBottomEdge) {
			action.params.vector.z = Math.abs(action.params.vector.x + 1) * -2
		}
		action.params.forceMaxSpeed = true;
	}
	return action;
}

Peasant.prototype.act = function(action) {
	if (!action) {
		return;
	}
	switch(action.type) {
		case 'roll':
			this.roll(action.params);
			break;
		case 'brake':
			this.brake();
			break;
	}
}

Peasant.prototype.roll = function(params) {
	var vector = params.vector.unit();
	var maxTorque = this.game.getMaxControllerTorque();
	var minTorque = -minTorque;
	var torqueX = vector.z * maxTorque;
	var torqueZ = -(vector.x * maxTorque);
	torqueX = torqueX > maxTorque ? maxTorque : torqueX < minTorque ? minTorque : torqueX;
	torqueZ = torqueZ > maxTorque ? maxTorque : torqueZ < minTorque ? minTorque : torqueZ;
	var scale = (Math.random() * 0.5) + 0.5;
	if (!params.forceMaxSpeed) {
		torqueX *= scale;
		torqueZ *= scale;
	}
	this.ball.applyTorque(torqueX, 0, torqueZ);
}

Peasant.prototype.brake = function() {
	var momentOfInertia = 2/5 * this.ball.body.mass * Math.pow(this.ball.body.shapes[0].radius, 2);
	var angVel = this.ball.body.angularVelocity;
	var timeStep = this.game.getStep();
	var maxTorque = this.game.getMaxControllerTorque();
	var minTorque = -minTorque;
	var torqueX = -(momentOfInertia * (angVel.x) / timeStep) * 0.1;
	var torqueZ = -(momentOfInertia * (angVel.z) / timeStep) * 0.1;
	torqueX = torqueX > maxTorque ? maxTorque : torqueX < minTorque ? minTorque : torqueX;
	torqueZ = torqueZ > maxTorque ? maxTorque : torqueZ < minTorque ? minTorque : torqueZ;
	this.ball.applyTorque(torqueX, 0, torqueZ);
}

Peasant.prototype.newTargetPointGoal = function(perception) {
	var groundDimension = this.game.getGround().aiDimension;
	var groundDimensionX = groundDimension.x - 8;
	var groundDimensionZ = groundDimension.z - 8;
	var goal = {
		type: 'targetPoint',
		params: {
			target: new CANNON.Vec3(
				(Math.random() * groundDimensionX) - groundDimensionX/2 , 
				0,
				(Math.random() * groundDimensionZ) - groundDimensionZ/2
			),
			duration: Math.random() * 15,
			startTime: new Date().getTime()
		}	
	};
	this.goals.push(goal);		
}

module.exports = Peasant;