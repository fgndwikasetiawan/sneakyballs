var CANNON = require('cannon');
var Materials = require('./materials.js');
var Utils = require('./utils.js');

var HIT_DURATION = 5000;
var DEFAULT_RADIUS = 1;

function Ball(position, rotation, material, mass, onDeath, deathZoneFunction) {
	initializeBody(position, rotation, material, mass, this);
	this.id = this.body.id;
	this.onGroundFlag = false;
	this.paused = false;
	this.isTicking = false;
	this.isDead = false;
	this.onDeath = onDeath;
	this.gotHit = null;
	this.isInDeathZone = deathZoneFunction;
	this.onTickResume = null;
	initializeCollisionListener(this);
	initializeCallbacks(this);
}


Ball.prototype.isOnGround = function() {
	if (this.onGroundFlag) {
		var contacts = this.body.world.contacts;
		for (var i in contacts) {
			var c = contacts[i];
			if (c.bi == this.body) {
				if (c.bj.isGround) {
					return true;
				}
			}
			else if (c.bj == this.body) {
				if (c.bi.isGround) {
					return true;
				}
			}
		}
		this.onGroundFlag = false;
	}
	return false;
}

Ball.prototype.jump = function() {
	if (this.isOnGround()) {
		this.body.applyForce(new CANNON.Vec3(0, 1000, 0), this.body.position);
	}
}

Ball.prototype.startTicking = function(duration, explosionCallback) {
	this.isTicking = true;
	this.tickDuration = duration;
	this.explosionTick = true;
	this.explosionCallback = explosionCallback;
	setTimeout(tick, duration/6, this, duration/6); //urgh
}

Ball.prototype.applyTorque = function(torqueX, torqueY, torqueZ) {
	this.body.torque = new CANNON.Vec3(torqueX, torqueY, torqueZ);
}

Ball.prototype.getBallInfo = function() {
	var ballInfo = {id: this.id, p: this.body.position, q: this.body.quaternion};
	if (this.explosionTick) {
		ballInfo.t = 1;
	};
	if (this.gotHit) {
		ballInfo.h = 1;
	}
	return ballInfo;
}

Ball.prototype.pause = function() {
	this.paused = true;
}

Ball.prototype.resume = function() {
	if (this.onTickResume !== null) {
		this.onTickResume();	
		this.onTickResume = null;
	}
	this.paused = false;
}

Ball.prototype.reinitialize = function() {
	this.onGroundFlag = false;
	this.paused = false;
	this.isTicking = false;
	this.isDead = false;
	this.gotHit = null;
	this.body.velocity = new CANNON.Vec3(0, 0, 0);
}

///////////////////////////////////////////////////////////////////////////
//___private functions																	///
///////////////////////////////////////////////////////////////////////////


function tick(ball, deltaTime) {
	if (ball.isInDeathZone(ball.body.position)) {
		ball.explosionTick = false;
		return;
	}
	ball.explosionTick = !ball.explosionTick;
	ball.tickDuration -= deltaTime;
	var nextTick = ball.tickDuration / 6;
	nextTick = nextTick > 100 ? nextTick : 100;
	if (ball.tickDuration > 0) {
		if (!ball.paused) {
			ball.tickCallbackId = setTimeout(tick, nextTick, ball, nextTick); //arrrghh
		}
		else {
			ball.onTickResume = function(){ball.tickCallbackId = setTimeout(tick, nextTick, ball, nextTick)}; //what the fuuuuck??!
		}
	}
	else {
		ball.isTicking = false;
		ball.explosionTick = false;
		ball.explosionCallback();
	}
}

function initializeBody(position, rotation, material, mass, ball) {
	position = position? position : CANNON.Vec3.ZERO;
	rotation = rotation? rotation : CANNON.Quaternion.ZERO;
	material = material? material : Materials.GLASS;
	mass = mass? mass : 1;
	ball.body = new CANNON.Body({mass: mass, material: material});
	ball.body.addShape(new CANNON.Sphere(DEFAULT_RADIUS));
	ball.body.position = position;
	ball.body.collisionGroup = Utils.CollisionFilterGroups.BALL;
	ball.body.collisionMask = Utils.CollisionFilterMasks.ALL;
	ball.body.angularDamping = 0.8;
	ball.body.ballObject = ball;
}

function initializeCollisionListener(ball) {
	ball.body.addEventListener('collide', 
		function(event){
			var bj = event.contact.bj;
			var bi = event.contact.bi;
			if (event.contact.bj.isGround) {
				ball.onGroundFlag = true;
			}
			if (bj.explosionObject) {
				ball.gotHit = {time: new Date().getTime(), by: bj.explosionObject.player};
			}
			else if (bj.ballObject && bj.ballObject != ball) {
				if (bj.ballObject.player) {
					ball.gotHit = {
						time: new Date().getTime(),
						by: bj.ballObject.player};
				}
				else if (bj.ballObject.gotHit && bj.ballObject.gotHit.by != ball.player) {
					ball.gotHit = {	
						time: bj.ballObject.gotHit.time,  
						by: bj.ballObject.gotHit.by
					};
				}
			}
			else if (bi.ballObject && bi.ballObject != ball) {
				if (bi.ballObject.player) {
					ball.gotHit = {
						time: new Date().getTime(), 
						by: bi.ballObject.player};
				}
				else if (bi.ballObject.gotHit && bi.ballObject.gotHit.by != ball.player) {
					ball.gotHit = {
						time: bi.ballObject.gotHit.time,
						by: bi.ballObject.gotHit.by
					};
				}
			}
		});
}

function initializeCallbacks(ball) {
	ball.body.preStep = function() {
		//check gotHit object
		if (ball.gotHit) {
			if (!ball.isInDeathZone(ball.body.position)) {
				//hanya cek durasi gotHit apabila diluar death zone 
				if (new Date().getTime() - ball.gotHit.time > HIT_DURATION) {
					ball.gotHit = null;
				}
			}
		}
	}
	ball.body.postStep = function() {
		//check if dieded
		if (!ball.isDead && ball.body.position.y < -30) {
			//console.log("ball " + ball.id + " dieded, lel");
			ball.isDead = true;
			if (ball.onDeath) {
				ball.onDeath();
			}
		}
	}
}


module.exports = Ball;