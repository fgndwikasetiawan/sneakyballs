var CANNON = require('cannon');

/*
	Gimana caranya dokumentasiin javascript?
	gini aja ya?
*/

/** 
 *	@constructor Explosion
 * Membuat sebuah body dengan bentuk sphere pada (position)
 * dengan radius awal 0.01 dan membesar hingga (maxRadius) 
 * selama (duration)
 * n.b. radius dihitung dengan menggunakan fungsi kuadratik
 */
function Explosion(player, position, maxRadius, duration, onFinish) {
	initializeBody(position, this);
	this.id = this.body.id;
	this.onFinish = onFinish;
	var sqrtRadius = Math.sqrt(maxRadius);
	this.calculateRadius = function(t) {
		return maxRadius - Math.pow((t * sqrtRadius / duration) - sqrtRadius, 2);
	}
	this.createdTime = new Date().getTime();
	this.duration = duration;
	var _this = this;
	this.body.preStep = function() {
		var t = new Date().getTime() - _this.createdTime;
		_this.body.shapes[0].radius = _this.calculateRadius(t);
		_this.body.shapes[0].updateBoundingSphereRadius();
		_this.body.updateBoundingRadius();
		if (t >= _this.duration) {
			_this.body.preStep = null;
			_this.body.postStep = function() {
				_this.onFinish();
			}
		}	
	}
	this.player = player;
	this.body.explosionObject = this;
}

Explosion.prototype.getExplosionInfo = function() {
	return {id: this.id, p: this.body.position, r: this.body.shapes[0].radius};
}

//--------------------------------------------------------------
//__private functions
//--------------------------------------------------------------


function initializeBody(position, explosion) {
	explosion.body = new CANNON.Body({type: CANNON.Body.KINEMATIC});
	explosion.body.addShape(new CANNON.Sphere(0.01));
	explosion.body.position = position;
}

module.exports = Explosion;