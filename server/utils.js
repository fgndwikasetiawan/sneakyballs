var CANNON = require('cannon');

var Utils = {};
Utils.CollisionFilterGroups = {
	GROUND: 1,
	BALL: 2
};
Utils.CollisionFilterMasks = {
	ALL: 0xffffff
}

module.exports = Utils;