var socket;
var camera;
var renderer;
var scene;
var isPaused;
var ground;
var background;
var light, light2, light3;
var balls = [];
var explosions = [];
var keyDown = [];
var pauseOverlay;
var pauseOverlayLabel;
var pauseOverlayInfo;
var playerList;
var playersSidebar;
var roundTimeDisplay;

var Colors = {
	WHITE: 0xffffff, //white
	ORANGE: 0xff8800, //orange
	YELLOW: 0xffff00, //yellow
	YELLOW_GREEN: 0xaaff00, //yellowish-green
	GREEN: 0x00cc00, //green
	CYAN: 0x00ffff, //cyan
	VIOLET: 0x8800ff, //violet
	BLUE: 0x0033dd, //blue
	MAGENTA: 0xff00ff, //magenta
	PINK: 0xffaaaa  //pink 
}

var ColorsArray = [
	Colors.WHITE, 
	Colors.ORANGE,
	Colors.YELLOW,
	Colors.YELLOW_GREEN,
	Colors.GREEN,
	Colors.CYAN,
	Colors.VIOLET,
	Colors.BLUE,
	Colors.MAGENTA,
	Colors.PINK 
];

var Textures = {
	DEFAULT: [
		THREE.ImageUtils.loadTexture('/images/smile1.png'),
		THREE.ImageUtils.loadTexture('/images/smile2.png'),
		THREE.ImageUtils.loadTexture('/images/smile3.png'),
		THREE.ImageUtils.loadTexture('/images/smile4.png')
	]
}

var BallMaterials = {
	DEFAULT: new THREE.MeshLambertMaterial({map: Textures.DEFAULT[0]}),
	TICKING: new THREE.MeshBasicMaterial({color: 0xff0000}),
	GOT_HIT: new THREE.MeshBasicMaterial({color: 0xaa00ff, wireframe: true}),
	EXPERIMENTAL: null
}

var ExplosionMaterials = {
	DEFAULT: new THREE.MeshBasicMaterial({color: 0xffaa00})
}
ExplosionMaterials.DEFAULT.transparent = true;
ExplosionMaterials.DEFAULT.opacity = 0.5;
ExplosionMaterials.DEFAULT.blending = THREE.AdditiveBlending;

window.onload = function() {
	initializeElements();
	initialize3DObjects();
	initializeRenderer();
	initializeSocket();
	
	draw();
}

window.onkeypress = function(event) {
	if (event.charCode == 80 || event.charCode == 112) {
		if (isPaused) {
			socket.emit('resume', {});
		}
		else {
			socket.emit('pause', {});
		}
	}
	else if (event.charCode == 82 || event.charCode == 114) {
		socket.emit('restart', {});
	}
}

function initializeElements() {
	pauseOverlay = document.getElementById('pauseOverlay');
	pauseOverlay.style.height = window.innerHeight + 'px';
	playersSidebar = document.getElementById('playersSidebar');
	playerList = document.getElementById('playerList');
	pauseOverlayLabel = document.getElementById('pauseOverlayLabel');
	pauseOverlayInfo = document.getElementById('pauseOverlayInfo');
	roundTimeDisplay = document.getElementById('roundTimeDisplay');
}

function initialize3DObjects() {
	camera = new THREE.PerspectiveCamera(75, window.innerWidth/window.innerHeight, 0.01, 1000);
	renderer = new THREE.WebGLRenderer();
	scene = new THREE.Scene();
	ground = new THREE.Mesh(new THREE.BoxGeometry(50.5, 1000, 50.5), new THREE.MeshPhongMaterial({color: 0xdddddd}));
	ground.position.set(0, -500, 0);
	//ground.receiveShadow = true;
	scene.add(ground);
	light = new THREE.PointLight(0xffffff, 1);
	light.position.set(0, 30, 0);
	scene.add(light);
	light2 = new THREE.PointLight(0xffffff);
	light2.position.set(0, -15, 30);
	scene.add(light2);
	camera.position.set(0, 30, 28);
	camera.lookAt(new THREE.Vector3(0, 0, 10));
	background = new THREE.Mesh(new THREE.BoxGeometry(10000, 1, 10000), new THREE.MeshLambertMaterial({color: 0xffffff}));
	background.position.set(0, -50, 0);
	scene.add(background);
	
	for (var i=0; i<8; i++) {
		var randomObject = new THREE.Mesh(new THREE.BoxGeometry(Math.random() * 9 + 1, Math.random() * 50 + 70, Math.random() * 9 + 1),
			new THREE.MeshLambertMaterial({color: 0xaaaaaa})
		);
		randomObject.position.set(Math.random() * 60 - 30,
											-50,
											(Math.random() * 5 + 30) * (i % 2 == 0 ? -1 : 1)
		);
		console.log(randomObject.position);
		scene.add(randomObject);
	}
	
	for (var i=0; i<5; i++) {
		randomObject = new THREE.Mesh(new THREE.BoxGeometry(Math.random() * 5 + 5, Math.random() * 50 + 70, Math.random() * 5 + 5),
			new THREE.MeshLambertMaterial({color: 0xaaaaaa})
		);
		randomObject.position.set((Math.random() * 5 + 30) * (i % 2 == 0 ? -1 : 1),
											-50,
											Math.random() * 60 - 30
		);
		console.log(randomObject.position);
		scene.add(randomObject);
	}

		
	scene.fog = new THREE.FogExp2(0xd0f0ff, 0.012);
}

function initializeRenderer() {
	renderer.setSize(window.innerWidth, window.innerHeight);
	renderer.setClearColor(0x440077);
	//renderer.shadowMapEnabled = true;
	var canvas = renderer.domElement;
	document.getElementById('canvasContainer').appendChild(canvas);
}

function initializeSocket() {
	socket = io(undefined, {transports: ['websocket']});
	socket.emit('watcher', {});
	socket.on('u', handleUpdates);
	socket.on('e', handleExplosion);
	socket.on('ee', handleExplosionEnd);
	socket.on('d', handleBallDeath);
	socket.on('pc', handlePlayerConnect);
	socket.on('ppg', handlePlayerGetPowerup);
	socket.on('ppu', handlePlayerUsePowerup);
	socket.on('pd', handlePlayerDisconnect);
	socket.on('ps', handlePlayerScores);
	socket.on('pause', handlePause);
	socket.on('resume', handleResume);
	socket.on('end', handleRoundEnd);
}

function handlePause(data) {
	isPaused = true;
	if (data.type == 'init') {
		pauseOverlayLabel.innerText = 'Waiting for players';
	}
	else {
		pauseOverlayLabel.innerText = 'Game Paused';
	}
	pauseOverlayInfo.innerText = 'Press \'P\' to start the game';
	pauseOverlay.style.display = 'block';
}

function handleResume() {
	isPaused = false;
	pauseOverlay.style.display = 'none';
}

function handleRoundEnd(data) {
	isPaused = true;
	pauseOverlayLabel.innerText = 'Round ended! The winner is ' + data.winner + '!!!';
	pauseOverlayInfo.innerText = 'Press \'R\' to play again';
	pauseOverlay.style.display = 'block';
	for (var i in balls) {
		console.log('removing ball ' + i);
		scene.remove(balls[i]);
	}
	balls = [];
}

function handleUpdates(updates) {
	if (updates.t || updates.t == 0) {
		changeRoundTime(updates.t);
	}
	if (updates.bi) {
		var ballInfos = updates.bi;
		for (var i in ballInfos) {
			var ball = balls[ballInfos[i].id];
			if (!ball) {
				var defaultMaterial = new THREE.MeshLambertMaterial(
													{
														map: Textures.DEFAULT[Math.round(Math.random() * (Textures.DEFAULT.length-1))], 
														color: ColorsArray[Math.round(Math.random() * (ColorsArray.length-1))]
													});
				ball = new THREE.Mesh(new THREE.SphereGeometry(1, 32, 32), defaultMaterial);
				ball.defaultMaterial = defaultMaterial;
				//ball.castShadow = true;
				balls[ballInfos[i].id] = ball;
				scene.add(ball);
			}
			ball.position.copy(ballInfos[i].p);
			ball.quaternion.copy(ballInfos[i].q);
			if (ballInfos[i].t == 1) {
				ball.material = BallMaterials.TICKING;
			}
			// else if (ballInfos[i].h != null) {
			// 	ball.material = BallMaterials.GOT_HIT;
			// }
			// else if (ballInfos[i].id == 20) {
			// 	ball.material = BallMaterials.GOT_HIT;
			// }
			else {
				ball.material = ball.defaultMaterial;
			}
		}
	}
	if (updates.ei) {
		var explosionInfos = updates.ei;
		for (var i in explosionInfos) {
			var explosion = explosions[explosionInfos[i].id];
			if (!explosion) {
				explosion = new THREE.Mesh(new THREE.SphereGeometry(explosionInfos[i].r, 16, 16), ExplosionMaterials.DEFAULT);
				explosions[explosionInfos[i].id] = explosion;
				scene.add(explosion); 
			}
			explosion.position.copy(explosionInfos[i].p);
			explosion.geometry = new THREE.SphereGeometry(explosionInfos[i].r, 16, 16);
		}
	}
}

function handleExplosion(ballId) {
	console.log("BOOM!");
}

function handleExplosionEnd(explosionId) {
	scene.remove(explosions[explosionId]);
	explosions.splice(explosionId, 1);
}

function handleBallDeath(ballId) {
	console.log("a ball dieded lel");
}

function handlePlayerConnect(player) {
	var playerItem = document.createElement('li');
	playerItem.id = 'playerListItem-' + player.id;
	playerItem.appendChild(document.createElement('img'));
	playerItem.appendChild(document.createElement('span'));
	playerItem.children[1].innerText = player.name + ': 0';
	playerList.appendChild(playerItem);
}

function handlePlayerDisconnect(playerId) {
	var playerItem = document.getElementById('playerListItem-' + playerId);
	playerList.removeChild(playerItem);
}

function handlePlayerScores(playerScores) {
	for(var i in playerScores) {
		var playerItem = document.getElementById('playerListItem-' + playerScores[i].id);
		var playerName = playerItem.innerText.split(':')[0];
		playerItem.children[1].innerText = playerName + ': ' + playerScores[i].s;
	}
}

function handlePlayerGetPowerup(data) {
	var playerItem = document.getElementById('playerListItem-' + data.id);
	switch(data.p) {
		case 'swap':
			playerItem.children[0].src = '/images/powerup_swap.png';
			break;
		case 'instabomb':
			playerItem.children[0].src = '/images/powerup_instabomb.png';
			break;
	}
}

function handlePlayerUsePowerup(data) {
	var playerItem = document.getElementById('playerListItem-' + data.id);
	playerItem.children[0].src = '';
}

function changeRoundTime(time) {
	roundTimeDisplay.innerText = time;
}

function draw() {
	renderer.render(scene, camera);
	requestAnimationFrame(draw);
}

function start() {
	setInterval(function(){
		socket.emit('a', {k: keyDown});
	}, 1000/60);
}
