var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Game = require('./server/game.js');

const port = 8080	;

app.get('/', function(req, res) {
	res.sendFile(__dirname + '/client/game.html');
});

app.get('/else/:name', function(req, res) {
	res.sendFile(__dirname + '/client/libs/' + req.params.name);
});

app.get('/js/:name', function(req, res) {
	res.sendFile(__dirname + '/client/js/' + req.params.name);	
});

app.get('/css/:name', function(req, res) {
	res.sendFile(__dirname + '/client/css/' + req.params.name);
});

app.get('/fonts/:dir/:name', function(req, res) {
	res.sendFile(__dirname + '/client/fonts/' + req.params.dir + '/' + req.params.name);
});

app.get('/images/:name', function(req,res) {
	res.sendFile(__dirname + '/client/images/' + req.params.name);
})


http.listen(port, function() {
	console.log('listening on port ' + port);
})

io.on('connection', function(socket) {
	console.log('someone connected');
	socket.on('controller', function(playerName) {
		console.log('(controller)');
		Game.addPlayer(socket, playerName);
	});
	socket.on('watcher', function(data) {
		console.log('(watcher)');
		Game.addWatcher(socket);	
	})
	socket.on('play', function(data) {
		Game.resume();
	});
	//Game.resume();
});

